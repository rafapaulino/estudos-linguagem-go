package main

import (
	"fmt"
	"log"
	"net/http" //this package provides HTTP client and server implementations
	"time"
)

func main() {
	http.HandleFunc("/", handler)
	log.Println("listening...")
	err := http.ListenAndServe(":9999", nil)
	if err != nil {
		panic(err)
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello. The time is : "+time.Now().Format(time.RFC850))
}
