;
(function (window, document, $, undefined) {
    'use strict';

    var app = (function () {

        var $private = {};
        var $public = {};
        
       
        /*
         * resolvendo os conflitos entre o Twitter Bootstrap e JqueryUI 
         * http://www.ryadel.com/en/using-jquery-ui-bootstrap-togheter-web-page/
         */
        $public.jQueryUIandTwitterBootstrapConflict = function() {
            // Change JQueryUI plugin names to fix name collision with Bootstrap.
            $.widget.bridge('uitooltip', $.ui.tooltip);
            $.widget.bridge('uibutton', $.ui.button); 
        };
        
        return $public;
    })();

    // Global
    window.app = app;
    app.jQueryUIandTwitterBootstrapConflict();

})(window, document, jQuery);